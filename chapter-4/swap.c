#include <stdio.h>

#define swap(T,A,B) {T temp = A; A = B; B = temp;}

int main(){
    int a = 'a';
    int b = 'G';
    printf("%d:%d\n", a, b);
    swap(int, a, b);
    printf("%c:%c\n", a, b);

    return 0;
}
