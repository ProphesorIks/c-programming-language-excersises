#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define MAXOP 100
#define NUMBER '0'
#define MAXVAL 100
#define BUFSIZE 100

int sp = 0;
double val[MAXVAL];
char buf[BUFSIZE];
int bufp = 0;

int getopt(char []);
void push(double);
double pop(void);
int getch(void);
void ungetch(int);

// Reverse polish calculator
int main(){
    int type;
    double op2;
    char s[MAXOP];
    printf("gothere0");
    while((type = getopt(s)) != EOF){
       switch (type) {
            case NUMBER:
                push(atof(s));
                break;
            case '+':
                push(pop() + pop());
                break;
            case '*':
                push(pop() * pop());
                break;
            case '-':
                op2 = pop();
                push(pop() - op2);
                break;
            case '/':
                op2 = pop();
                if(op2 != 0.0){
                    push(pop() / op2);
                }else{
                    printf("error: zero division");
                }
                break;
            case '\n':
                printf("\t%.8g\n", pop());
                break;
            default:
                printf("unknown command %s\n", s);
                break;
       }
    }
    while(sp > 0){
        printf("%d:\t%g\n",sp, pop());
    }
    return 0;
}

//int getop(char s[])
//{
//    int i, c;
//    printf("gothere");
//    while ((s[0] = c = getch()) == ' ' || c == '\t')
//        ;
//    s[1] = '\0';
//    if (!isdigit(c) && c != '.'){
//        return c; /* not a number */
//    }
//    i = 0;
//    if (isdigit(c)){ /* collect integer part */
//        while (isdigit(s[++i] = c = getch()))
//            ;
//    }
//    if (c == '.'){ /* collect fraction part */
//        while (isdigit(s[++i] = c = getch()))
//            ;
//    }
//    s[i] = '\0';
//    if (c != EOF){
//        ungetch(c);
//    }
//    return NUMBER;
//}

void push(double f){
    if(sp < MAXVAL){
        val[sp++] = f;
    }else{
        printf("Error, stack full, can't push %g\n", f);
    }
}

double pop(){
    if(sp > 0){
        return val[--sp];
    }else{
        printf("Error, stack empty\n");
        return 0.0;
    }
}



int getch(void){
    printf("getched:%d", bufp);
    return bufp > 0 ? buf[--bufp] : getchar();
}

void ungetch(int c){
    if(bufp > BUFSIZE)
        printf("ungetch too many characters\n");
    else
        buf[bufp++] = c;
}
