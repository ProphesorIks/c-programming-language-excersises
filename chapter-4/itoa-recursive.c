#include <stdio.h>
#include <limits.h>
#include <string.h>

#define MAX_LENGHT 100

void itoa(int i, char s[]);
void itoa_r(int i, char s[]);

int main(){
    for(int i = -10; i < 2400; i+= 117){
        char s[MAX_LENGHT];
        itoa(i, s);
        printf("%4d:%4s\n", i, s);
    }
    return 0;
}

void itoa(int n, char s[]){
   s[0] = '\0';
   itoa_r(n, s);
}

void itoa_r(int n, char s[]){
    int remainder, l;

    if(n / 10)
        itoa(n / 10, s);

    l = strlen(s);
    if(n / 10 == 0 && n < 0)
        s[l++] = '-';

    remainder = n % 10;
    s[l++] = ((n < 0) ? -remainder : remainder) + '0';
    s[l++] = '\0';
}
