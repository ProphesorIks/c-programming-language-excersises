#include <stdio.h>
#include <ctype.h>
#include <math.h>

double atof(char *);

int main(){
    printf("%g\n", atof("-10.1002"));
    printf("%g\n", atof("10e+2"));
    return 0;
}

double atof(char s[]){
    double val, power, power_ten;
    int i, sign, n_sign;

    for(i = 0; isspace(s[i]); i++)
        ;
    sign = (s[i] == '-') ? -1 : 1;
    if(s[i] == '+' || s[i] == '-')
        i++;
    for(val = 0.0; isdigit(s[i]); i++)
        val = 10.0 * val + (s[i] - '0');
    if(s[i] == '.')
        i++;
    for(power = 1.0; isdigit(s[i]); i++){
        val = 10.0 * val + (s[i] - '0');
        power *= 10;
    }
    
    if(s[i] == 'e' || s[i] == 'E')
        i++;
    n_sign = (s[i] == '-') ? -1 : 1;
    if(s[i] == '+' || s[i] == '-')
        i++;
    for(power_ten = 0.0; isdigit(s[i]); i++){
        power_ten = 10.0 * power_ten + (s[i] - '0');
    }

    power_ten = pow(10, power_ten);
    if(n_sign == -1){
        power_ten = 1.0 / power_ten;
    }

    return (sign * val / power) * power_ten;
}
