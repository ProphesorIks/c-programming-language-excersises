#include <stdio.h>
#include <string.h>

#define MAX_LINE 100


void strrev(char s[]);
void strrev_r(char s[], int i, int j);
int get_line(char s[]);

int main(){
    char s[MAX_LINE];
    get_line(s);
    printf("%s\n", s);
    strrev(s);
    printf("%s\n", s);
    return 0;
}

void strrev(char s[]){
    strrev_r(s, 0, strlen(s) -1); 
}

void strrev_r(char s[], int i, int j){
    int swap = s[i];
    s[i] = s[j];
    s[j] = swap;

    if(i < j)
        strrev_r(s, ++i, --j);
}

int get_line(char s[]){
    int i = 0;
    int c = 0;
    while((c = getchar()) != EOF && c != '\n')
       s[i++] = c;
    if(c == '\n')
        s[i++] = '\n';
    s[i] = '\0';
    return i;
}
