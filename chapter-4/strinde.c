#include <stdio.h>
#include <string.h>

int strindex(char[], char[]);

int main(){
    printf("%d\n", strindex("Hello world!", "H"));
    return 0;
}

int strindex(char s[], char t[]){
    int i, j, k;
    int s_len = strlen(s);
    for(i = s_len; i >= 0; i--){
        for(j = i, k = 0; t[k] != '\0' && s[j] == t[k]; j++, k++)
            ;
        if(k > 0 && t[k] == '\0')
            return i;
        
    }
    return -1;
}
