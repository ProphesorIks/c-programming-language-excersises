#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#define MAX_WORD_SIZE 1000
#define BUFSIZE 100

struct list_item {
    char * line;
    struct list_item * next;
};

void list_add(struct list_item **, struct list_item *);
void list_remove(struct list_item **, struct list_item *);
struct list_item * list_search(struct list_item **, char *);
void list_print(struct list_item **, char *);
int getch(void);
void ungetch(int);
int getword(char *);
char * ft_strdup(char *);
struct list_item *lalloc(void);

char buf[BUFSIZE];
int bufp;

int main()
{
    struct list_item *root = lalloc(); 
//    struct list_item *first = lalloc();
//    struct list_item *second = lalloc();
//    struct list_item *third = lalloc();
//    struct list_item *fourth = lalloc();
//    struct list_item *fifth = lalloc();
//
//    root->line = "CCC";
//    first->line = "BBB";
//    second->line = "AAA";
//    third->line = "ZZZ";
//    fourth->line = "aaa";
//    fifth->line = "A";

    char word[MAX_WORD_SIZE];
    while(getword(word) != EOF){
        struct list_item *line = lalloc();
        line->line = ft_strdup(word);
        list_add(&root, line);
    }

    //list_add(&root, third);
    //list_add(&root, fifth);

    list_print(&root, "->%s\n");

    struct list_item *test =  list_search(&root, "Hello");

    if(test != NULL)
        list_remove(&root, test);
    list_print(&root, "->%s\n");
    return 0;
}

void list_add(struct list_item **root, struct list_item *node)
{
    struct list_item **probe = root; 
    for(; (*probe) != NULL && (*probe)->line != NULL; probe = &((*probe)->next)){
        if(strcmp(node->line, (*probe)->line) < 0){
            node->next = (*probe);
            *probe = node;
            return;
        }else if(strcmp(node->line, (*probe)->line) == 0){
            printf("Item already in the list: %s\n", (*probe)->line);
            return;
        }
        if(NULL == (*probe)->next){
            (*probe)->next = node;
            return;
        }
    }
    *probe = node;
}

struct list_item * list_search(struct list_item **root, char *w)
{
    struct list_item *probe = *root;

    do{
        if(strcmp(probe->line, w) == 0){
            return probe;
        }
        probe = probe->next;
    }while(probe != NULL); 

    return NULL;

}

void list_remove(struct list_item **root, struct list_item *node)
{
    struct list_item **probe = root;
    for(; (*probe) != NULL; probe = &((*probe)->next)){
        if(*probe == node){
            *probe = ((*probe)->next); 
            free(node);
            return;
        }
    }
}

void list_print(struct list_item **root, char *format)
{
    struct list_item *probe = *root;

    do{
        printf(format, probe->line);
        probe = probe->next;
    }while(probe != NULL); 
}

char *ft_strdup(char *src)
{
    char *str;
    char *p;
    int len = 0;

    while (src[len])
        len++;
    str = malloc(len + 1);
    p = str;
    while (*src)
        *p++ = *src++;
    *p = '\0';
    return str;
}

int getword(char *word) 
{
    char* w = word;
    int c;
    int count = 0;
    while(isspace(c = getch()))
        ;
    if (c != EOF)
        *w++ = c;
    if(!isalpha(c)){
        *w = '\0';
        return c;
    }

    for(;count < MAX_WORD_SIZE; w++, count++)
        if(!isalpha(*w = getch()))
        {
            ungetch(*w);
            break;
        }
    *w = '\0';
    return word[0];
}

struct list_item * lalloc(void)
{
    return (struct list_item *) malloc(sizeof(struct list_item));
}

int getch(void)
{
    return (bufp > 0) ? buf[--bufp] : getchar();
}

void ungetch(int c)
{
    if(bufp >= BUFSIZE)
        printf(
                "Ungetch: too many chars to ungetch");
    else
        buf[bufp++] = c;
}
