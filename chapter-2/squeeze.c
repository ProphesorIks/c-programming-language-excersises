#include <stdio.h>

void squeeze(char [], char []);

int main(){
    char str1[] = "Hello";
    char str2[] = "The quick brown fox";
    char str3[] = "These truths we hold self evident";

    squeeze(str1, "World");
    squeeze(str2, "jumping over a lazy dog.");
    squeeze(str3, "ezxc");
    printf("%s\n", str1); 
    printf("%s\n", str2); 
    printf("%s\n", str3); 
    return 0;
}

void squeeze(char s1[], char s2[]){
    int i, j;
    for(i = j = 0; s1[i] != '\0'; i++){
        int found_match = 0;
        for(int k = 0; s2[k] != '\0'; k++){
           if(s1[i] == s2[k]){
                found_match = 1;
                break;
           }
        }
        if(!found_match)
            s1[j++] = s1[i];
    }
    s1[j] = '\0';
}
