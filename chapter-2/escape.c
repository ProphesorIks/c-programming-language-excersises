#include <stdio.h>
void escape(char[], char[]);
void unescape(char[], char[]);

int main(){
    char s1[255];
    char s2[] = "Hello \nWorld!\tHowdy..Ok~";
    char s3[255];
    escape(s2, s1);
    unescape(s1, s3);
    printf("%s\n",s1);
    printf("%s\n",s3);

    return 0;
}

void escape(char s[], char t[]){
    int i, j = 0;
    for(i = 0; s[i] != '\0'; i++){
        switch(s[i]){
            case '\n':
                t[j] = '\\';
                t[++j] = 'n';
                break;
            case '\t':
                t[j] = '\\';
                t[++j] = 't';
                break;
            case '\b':
                t[j] = '\\';
                t[++j] = 'b';
                break;
            default:
                t[j] = s[i];
                break;
        }
        j++;
    }
    t[j] = '\0';
}

void unescape(char s[], char t[]){
    int i, j = 0;
    for(i = 0; s[i] != '\0'; i++){
        if(s[i] == '\\'){
            switch(s[i+1]){
                case 'n':
                    t[j] = '\n';
                    i++;
                    break;
                case 't':
                    t[j] = '\t';
                    i++;
                    break;
                case 'b':
                    t[j] = '\b';
                    i++;
                    break;
                default:
                    t[j] = '\\';
                    break;
            }
        }else{
            t[j] = s[i];
        }
        j++;
    }
    t[j] = 0;
}
