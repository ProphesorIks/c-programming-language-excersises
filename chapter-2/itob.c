#include <stdio.h>
#include <limits.h>
#include <string.h>

#define abs(x) ( (x) > 0 ? (x): -(x))

char base(unsigned);
void reverse(char []);
void itob(int, char [], int, int);

int main(){
    char s[255];
    itob(42, s, 16, 8);
    printf("%s\n", s);
    return 0;
}

char base(unsigned i){
    if(i < 10){
        return '0' + i;
    }else{
        return 'a' + (i - 10);
    }
}

void itob(int n, char s[], int b, int w){
    int sign, i;

    if((sign = n) < 0)
        n = -n;
    i = 0;
    do{
        s[i++] = base(abs(n % b));
    }while((n /= b) != 0);
    
    while(i < w-1)
        s[i++] = '0';

    if(sign < 0){
        s[i++] = '-';
    }else{
        s[i++] = '0';
    }
    s[i] = '\0';
    reverse(s);
}

void reverse(char s[]){
    int len = strlen(s);

    int j = len - 1;
    for(int i = 0; i < len/2; i++, j--){
        char fc = s[i];
        char sc = s[j];

        s[j] = fc;
        s[i] = sc;
    }
    s[len+1] = '\0';
}
