#include <stdio.h>

signed any(char[], char[]);

int main(){
    printf("pos: %d\n", any("Hello", "World"));
    printf("pos: %d\n", any("Hello", "zxcv"));
    return 0;
}

signed any(char s1[], char s2[]){
    for(int i = 0; s1[i] != '\0'; i++)
        for(int j = 0; s2[j] != '\0'; j++)
            if(s1[i] == s2[j])
                return i;
    return -1;
}
