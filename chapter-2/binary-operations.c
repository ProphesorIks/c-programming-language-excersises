#include <stdio.h>

unsigned invert(int, int, int);
unsigned setbits(int, int, int, int);
unsigned rightrot(int, int);

int main(){
    printf("Setbits:\n");
    printf("%u\n", setbits(54,4,2,25));
    printf("%u\n", setbits(24450,8,5,1023));
    printf("Invert:\n");
    printf("%u\n", invert(55,4,3));

    return 0;
}


unsigned setbits(int x, int p, int n, int y){
    int ox = x;
    y = (~(~0 << n) & y) << (p - n + 1);
    x = (x >> p + 1) << p +1;
    x += y;
    ox &= ~(~0 << (p - n + 1));
    return x + ox;
}

unsigned invert(int x, int p, int n){
    unsigned xe = ~(~0 << p-n+1) & x;
    unsigned xb = (x >> p+1) << p +1;
    unsigned bm = ~(~0 << p+1) - ~(~0 << p-n+1);
    x = (~x & bm);
    return xb + x + xe;
}

unsigned rightrot(int x, int n){
    
}
