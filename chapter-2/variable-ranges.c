#include <stdio.h>
#include <limits.h>

/*
 * determine char, short, int and long ranges. Both signed and not;
 *
 */
void print_limits(int integer, int character, int short_integer, int long_integer, int u_integer, int u_character, int u_short_integer, int u_long_integer);

int main(){
    //Limits from limits.h
    print_limits(INT_MAX, CHAR_MAX, SHRT_MAX, LONG_MAX, UINT_MAX, UCHAR_MAX, USHRT_MAX, ULONG_MAX);
    return 0;
}

void print_limits(int integer, int character, int short_integer, int long_integer, int u_integer, int u_character, int u_short_integer, int u_long_integer){
    printf(
    "type\tunsigned:\tsigned\nint\t%d\t%d\nchar\t%d\t%d\nshort\t%d\t%d\nlong\t%d\%d\n",
    integer, character, short_integer, long_integer, u_integer, u_character, u_short_integer, u_long_integer);
                                        


}

