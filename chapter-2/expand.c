#include <stdio.h>

void expand(const char [], char []);

int main(){
    const char s0[] = "a-z";
    const char s1[] = "0-9";
    const char s2[] = "-0-0a-z";
    const char s3[] = "A-D";
    const char s4[] = "a-d-f";
    const char s5[] = "A-Zo-v-x";
    const char s6[] = "-a-z0-1-6a3bq-";
    char destination[1024] = "";
   
    expand(s0, destination);
    printf("%s -> %s\n", s0, destination);
    destination[0] = '\0';

    expand(s1, destination);
    printf("%s -> %s\n", s1, destination);
    destination[0] = '\0';

    expand(s2, destination);
    printf("%s -> %s\n", s2, destination);
    destination[0] = '\0';

    expand(s3, destination);
    printf("%s -> %s\n", s3, destination);
    destination[0] = '\0';

    expand(s4, destination);
    printf("%s -> %s\n", s4, destination);
    destination[0] = '\0';

    expand(s5, destination);
    printf("%s -> %s\n", s5, destination);
    destination[0] = '\0';

    expand(s6, destination);
    printf("%s -> %s\n", s6, destination);
    destination[0] = '\0';


    return 0;
}

void expand(const char s1[], char s2[]){
    int i = 0;
    int pos = 0;
    int endchar = 0;
    int beginchar = 0;
    int entered_expansion_flag = 0;

    while(s1[pos] != '\0'){
        entered_expansion_flag = 0;
        if(s1[pos] >= '0' && s1[pos] <= 'z'){
            beginchar = s1[pos];
            if(s1[pos + 1] == '-'){
                if(s1[pos + 2] != '-' && s1[pos + 2] != '\0'){
                    endchar = s1[pos + 2];
                    if(endchar > beginchar){
                        for(int j = beginchar; j < endchar+1; j++){
                           s2[i] = j;
                           i++;
                        }
                        pos++;
                    }
                }
            }
        }
        pos++;
    }
    s2[i] = '\0';
}
