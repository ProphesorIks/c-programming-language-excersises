#include <stdio.h>

int binsearch(int, int[], int);

int main(){
    int hay[] = {12,14,90,189,201};
    printf("%d\n",binsearch(400, hay, 5));
    return 0;
}

int binsearch(int x, int v[], int n){
    int low, high, mid;
    low = 0;
    high = n-1;
    mid = (low+high)/2;

    do{
        if(x < v[mid])
            high = mid-1;
        else
            low = mid+1;
        mid = (low+high)/2;

    }while(low < high && x != v[mid]);
    if(x == v[mid])
        return mid;
    return -1;
}
