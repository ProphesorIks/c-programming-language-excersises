/*
 *
 * Write a file reade to arrya loop without || or && 
 */
#include <stdio.h>
int get_line(char string[], int lim);
int main(){
    
    char line[10];
    get_line(line, 10);

    printf("Line is: \n%s",line);
    return 0;
}

int get_line(char s[], int lim){
    int c;
    int i;
    for(i = 0; i < lim-1; ++i){
        if((c=getchar()) != '\n'){
            if(c != EOF){
                s[i] = c;
            }else{
                break; 
            }
        }else{
            break; 
        }
    }

    if(c == '\n'){
        s[i] = '\n';
        ++i;
    }    
    s[i] = '\0';
   return i; 
}
