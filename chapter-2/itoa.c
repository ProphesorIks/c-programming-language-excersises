#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <raylib.h>

#define abs(x) ( (x) > 0 ? (x): -(x))
void itoa (int, char []);
void reverse(char[]);

int main(){
    char s2[1024];
    itoa(INT_MIN + 1,s2);
    printf("%s\n", s2);

    itoa(INT_MIN,s2);
    printf("%s\n", s2);


    //printf("%d\n", (INT_MIN)/10000000000 % 10);
    return 0;
}

void itoa(int n, char s[]){
    int sign, i;

    if((sign = n) < 0)
        n = -n;
    i = 0;
    do{
        s[i++] = abs(n % 10) + '0';
    }while((n /= 10) != 0);
    
    if(sign < 0)
        s[i++] = '-';
    s[i] = '\0';
    printf("%d\n", i);
    reverse(s);
}

void reverse(char s[]){ 
    int len = strlen(s);

    int j = len - 1;
    for(int i = 0; i < len/2; i++, j--){
        char fc = s[i];
        char sc = s[j];

        s[j] = fc; 
        s[i] = sc;
    }
    s[len+1] = '\0';
}
