/*
 * convert hex string to it's int value
 *
 */
#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <string.h>

long htoi(char []);
int hex_to_number(char c);

int main(){
    char t_0[] = "1A";
    char t_1[] = "F";
    char t_2[] = "BBd86";
    char t_3[] = "J0";

    long a_0 = htoi(t_0);
    long a_1 = htoi(t_1);
    long a_2 = htoi(t_2);
    long a_3 = htoi(t_3);

    printf(
        "%s -> %d\n"
        "%s -> %d\n"
        "%s -> %d\n"
        "%s -> %d\n",
        t_0,a_0,
        t_1,a_1,
        t_2,a_2,
        t_3,a_3
    );
    return 0;
}

long htoi(char s[]){
   long res = 0;
   long ln = strlen(s);

   for(int i = ln; i >= 0; i--){
        char c = s[i];
        int int_val= 0;
        if((int_val = hex_to_number(c)) != -1){
            res += pow(16, ln - i - 1) * int_val;
        }
   }
   return res;
}

int hex_to_number(char c){
    c = tolower(c);
    if(isxdigit(c)){
        const char hex[] = {'0', '1', '2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
        int i = 0;
        for(i = 0; c != hex[i]; i++);
                 
        return i; 
    }
}

