#include <stdio.h>

int lover(int c);

int main(){
    printf("%c\n", lover('K'));
    printf("%c\n", lover('a'));
    printf("%c\n", lover('2'));
    return 0;
}

int lover(int c){
    return (c >= 'A' && c <= 'Z') ? c + 'a' - 'A' : c;
}
