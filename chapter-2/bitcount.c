#include <stdio.h>

unsigned bitcount(int);
int main(){
    printf("%d\n", bitcount(61));
    printf("%d\n", bitcount(0));
    printf("%d\n", bitcount(3249));
    return 0;
}

unsigned bitcount(int x){
    unsigned b = 0;
    while(x != 00){
        x &= (x-1);
        b++;
    }
    return b;
}
