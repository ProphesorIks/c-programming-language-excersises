#include <stdio.h>
#include <string.h>

int get_line(char *);

int main(){
    char c[1000];
    while(get_line(c) != EOF)
        printf("%s", c);
    return 0;
}

int get_line(char *s){
    int c;
    char *orig = s;
    
    while((c = getchar()) != EOF && c != '\n')
        *s++ = c;
    if(c == '\n'){
        *s++ = c;
    }
    *s = '\0';
    if(c != EOF)
        return strlen(orig);
    return c;
}
