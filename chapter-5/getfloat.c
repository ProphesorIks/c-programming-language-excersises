#include <stdio.h>
#include <ctype.h>
#define BUFSIZE 100

char buf[BUFSIZE];
int bufp = 0;

int getch(void);
void ungetch(int c);
float getfloat(float *pn);

int main(){
    float floating= 0;
    while(getfloat(&floating) != EOF)
        printf("Got: %f\n", floating);
    return 0;
}

float getfloat(float *pn){
    float c, sign, c2, frac;
    
    while(isspace(c = getch()))
        ;
    if(!isdigit(c) && c != EOF && c != '+' && c != '-' && c != '.'){
        ungetch(c);
        return 0.0;
    }
    sign = (c == '-') ? -1 : 1;
    if(c == '+' || c == '-'){
        if(!isdigit(c2 = getch())){
            ungetch(c2); 
            ungetch(c);
            return 0.0;
        } else
            c = c2;
    }
    for(*pn = 0; isdigit(c); c = getch())
        *pn = 10 * *pn + (c - '0');
    if(c == '.'){
        int frac_len = 10;
        for(frac = 0.0; isdigit(c = getch()); frac_len *= 10)
            frac += (c - '0') / frac_len;
    }

    *pn += frac;
    *pn *= sign;
    if(c != EOF)
        ungetch(c);
    return c;
}

int getch(void){
    return (bufp > 0) ? buf[--bufp] : getchar();
}

void ungetch(int c){
    if(bufp >= BUFSIZE)
        printf("ungetch too many characters");
    else
        buf[bufp++] = c;
}
