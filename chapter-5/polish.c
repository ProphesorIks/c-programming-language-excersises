#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STACK_SIZE 100

void push(double);
double pop(void);
int command(char *);
void swap_top();
double peek();

double stack[STACK_SIZE];
int sp = 0;

int main(int argc, char *argv[]){
    while(--argc){
        char *argument = *++argv;
        if(!command(argument)){
            double number = atof(argument);
            push(number);
        }
    }
    printf("Result: %g\n", peek());
    return 0;
}

int command(char *s){
    double op2;
    if(strcmp("+", s) == 0){
       push(pop() + pop()); 
    }else if(strcmp("-", s) == 0){
       swap_top();
       push(pop() - pop());
    }else if(strcmp("*",s) == 0){
        push(pop() * pop());
    }else if(strcmp("/",s) == 0){
        op2 = pop();
        if(op2 != 0.0)
            push(pop()/op2);
        else
            printf("Error: division by 0\n");
    }else if(strcmp("peek", s) == 0){
        printf("peek: %g\n", peek());
    }else{
        return 0;
    }
    return 1;
}


double peek(){
    return stack[sp - 1];
}
void swap_top(){
   double s1 = pop();
   double s2 = pop();
   push(s1);
   push(s2);
}

void push(double f){
    if(sp < STACK_SIZE)
        stack[sp++] = f;
    else
        printf("Stack full, can't push %g\n", f);
}

double pop(){
    if(sp > 0)
        return stack[--sp];
    else
        return 0.0;
}
