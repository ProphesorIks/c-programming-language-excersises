#include <stdio.h>

#define MAX_LINE 1000

void strcat_p(char *, char *);

int main(){
    char a[MAX_LINE] = "Hello ";
    char b[MAX_LINE] = "world!";
    strcat_p(a,b);

    printf("%s\n",a);
    return 0;
}

void strcat_p(char *s, char *t){
    while(*s)
       s++; 
    while(*t)
        *s++ = *t++;
}
