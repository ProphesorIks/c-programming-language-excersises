#include <stdio.h>
#include <string.h>
#define MAX_LINE 1000

int strend_p(char *, char *);

int main(){
    char a[MAX_LINE] = "Hello";
    char b[MAX_LINE] = "ello";
    int ends = strend_p(a,b);

    printf("%d\n",ends);
    return 0;
}

int strend_p(char *s, char *t){
    int ls = strlen(s);
    int lt = strlen(t);

    for(int i = 0; i < lt; i++){
        if(*s+ls-i != *t+lt-i){ 
            printf("%d:%c:%c", i, *(s+ls-i), *t+lt-i);
            return 0;
        }
    }

    return 1;
}

