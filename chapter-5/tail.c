#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define ALLOCSIZE 10000
#define MAX_LINE_MEMORY 10 

static char allocbuf[ALLOCSIZE];
static char *allocp = allocbuf;
char *alloc(int n);
void afree(char *p);
int get_line(char *line);
void push_line_to_queue(char *line);
void print_last_lines(char **lines);

int line_max = 10;
int line_count = 0;
char *queue[MAX_LINE_MEMORY];

int main(int argc, char **argv){
    //---------get command line arguments------//
    if(argc == 2){
        int lines = atoi(argv[1]);
        if(lines < 0)
            lines = -lines;
        if(lines > MAX_LINE_MEMORY)
            lines = MAX_LINE_MEMORY;
        line_max = lines;
    }    
    //---------get lines and store them--------// 
    char temp_line[ALLOCSIZE];

    while(get_line(temp_line) > 0){
        push_line_to_queue(temp_line);
        line_count++;
    }

    printf("Done reading, now printing %d lines\n", line_count < MAX_LINE_MEMORY ? line_count : MAX_LINE_MEMORY);
    print_last_lines(queue);
    return 0;
}

char *alloc(int n){
    if(allocbuf + ALLOCSIZE - allocp >= n){
        allocp += n;
        return allocp - n;
    }else
        return NULL;
}

void afree(char *p){
    if(p >= allocbuf && p < allocbuf + ALLOCSIZE)
        allocp = p;
}

int get_line(char * line){
    int i, c;
    for(i = 0; ((c = getchar()) != EOF) && c != '\n' && i < ALLOCSIZE; i++)
        line[i] = c;  
    if(c == '\n')
        line[i++]= '\n';
    line[i]= '\0';
    return i;
}

void print_last_lines(char **line_queue){
    for(int i = 0; i < line_max; i++){
        printf("%s\n", *(line_queue+i));
    }
}

void push_line_to_queue(char *line){
    int len = strlen(line);
    line[len-1] = '\0';
    char * line_p = alloc(len);
    strcpy(line_p, line);
    queue[line_count] = line_p;
}
