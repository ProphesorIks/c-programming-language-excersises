#include <stdio.h>

int day_of_year(int, int, int);
void month_day(int, int, int *, int *);
static char daytab[2][13] = {
    {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
    {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
};
char (*daytab_p)[13] = daytab;

int main(){
    int year = 2020;
    int today = day_of_year(year, 10, 14);
    int month, day;
    month_day(year, today, &month, &day);
    
    printf("yyyy:mm:dd:yda\n%i:%02i:%02i:%03i\n", year, month, day, today);
    return 0;
}

int day_of_year(int year, int month, int day){
    if(month > 12 || day > 31)
        return -1;
    int i, leap;
    leap = year%4 == 0 && year%100 != 0 || year%400 == 0;
    for(i = 1; i < month; i++)
        day += *(*(daytab_p+leap)+i);
    return day;
}

void month_day(int year, int yearday, int *pmonth, int *pday){
   int i, leap;
   if(yearday > 366)
       return;
   leap = year%4 == 0 && year%100 != 0 || year%400 == 0;
   for(i = 1; yearday > *(*(daytab_p+leap)+i); i++)
       yearday -= *(*(daytab_p+leap)+i);
   *pmonth = i;
   *pday = yearday;
}
