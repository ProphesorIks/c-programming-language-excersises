#include <stdio.h>

void strncpy_p(char *, char *, int);
void strncat_p(char *, char *, int);
int strncmp_p(char *, char *, int);

int main(){
    char a[] = {"Hello"};
    char b[] = {"test"};
    char c[] = {"mankin1"};
    char d[] = {"Hellomankind"};
    strncpy_p(a,b,4);
    printf("cp:%s\n---\n", b);
     
    strncat_p(a,c,100);
    printf("ct:%s\n---\n", a);

    int comp = strncmp_p(a,d, 20);
    printf("cm:%d\n---\n", comp);
    return 0;
}

void strncpy_p(char *s, char *t, int n){
    while((*t++ = *s++) && --n)
        ;
    *t = '\0';
}

void strncat_p(char *s, char *t, int n){
    while(*s)
        s++;
    while((*t) && n--)
        *s++ = *t++;
    *s = '\0';
}

int strncmp_p(char *s, char *t, int n){
    for(; (*s == *t) && --n; s++, t++)
        if(*s == '\0')
            return 0;
    return *s - *t;
}
