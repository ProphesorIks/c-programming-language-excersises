#include <stdio.h>

int comp_igcase(char *s, char *t){
    for( ; *s == *t || !((*s - *t) % 32); s++, t++)
        if(*s == '\0')
            return 0;
    return *s - *t;
} 

int main(){
    printf("%d\n", comp_igcase("Hello", "hello"));
    printf("%d\n", comp_igcase("Nigger", "Nigger"));
    printf("%d\n", comp_igcase("NIGGER", "Niggerio"));
    return 0;
}
