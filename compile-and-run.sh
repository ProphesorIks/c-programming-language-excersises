#!/bin/bash
#This program compiles and runs c program's binary
set -e
file_name=$1

file_name_no_extension=${file_name%.*}

gcc $file_name -o "$file_name_no_extension.out" -lm -pedantic -std=c99

echo "$file_name was compiled. Now it will be run."

./"$file_name_no_extension.out" "${@:2}"
