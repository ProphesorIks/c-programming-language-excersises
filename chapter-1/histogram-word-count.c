#include <stdio.h>

#define IN 1
#define OUT 0
#define MAX_LENGTH 32

/* Prints a histogram of words */
int main()
{
	int nword[MAX_LENGTH];
	int c = 0;
	int state = OUT;
	int wl = 0;
	int ml = 0;

	for(int i = 0; i < MAX_LENGTH; i++)
	{
		nword[i] = 0;
	}

	while((c = getchar()) != EOF)
	{
		if(' ' == c || '\t' == c || '\n' == c)
		{
			if(IN == state)
			{
				state = OUT;
				//if(MAX_LENGTH >= wl)
					nword[wl]++;
					wl = 0;
			}
		}
		else
		{
			if(ml <= ++wl)
				ml = wl;

			state = IN;
		}
	}

	//print the histogram
	for(int i = 0; i < ml; i++)
	{
		{
			printf("%2d", i);
			for(int j = 0; j < nword[i]; j++)
			{
				printf("-");
			}
			printf("\n");
		}
	}
}
