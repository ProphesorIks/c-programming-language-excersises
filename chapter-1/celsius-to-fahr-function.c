#include <stdio.h>

/* this program converts celcius to fahrenheit */

float fahr_to_cel(int t_cel);

int main()
{
	int lower = -60;
	int upper = 300;
	int step = 20;
	
	for(int i = lower; i < upper; i += step)
		printf("%3i:%3.2f\n",i,fahr_to_cel(i));
	return 0;
}

float fahr_to_cel(int t_cel)
{
	return t_cel * (9.0/5.0) + 32.0; 
}
