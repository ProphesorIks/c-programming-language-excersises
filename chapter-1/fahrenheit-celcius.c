#include <stdio.h>

/* print Fahrenheit- Celcius table */

main()
{
	float lower = 0;
	float fahr = lower;
	float celcius = 0;
	float upper = 300;
	float step = 20;

	printf("%3s %6s", "fahr", "celcius\n");

	while(fahr <= upper) {
		celcius = 5 * (fahr - 32) / 9;
		printf("%3.0f %6.1f\n", fahr, celcius);
		fahr += step;
	}
}
