#include <stdio.h>

int main()
{
	int c = 0;
	long nb = 0;
   	long nt = 0;
   	long nn = 0;
	
	while((c = getchar()) != EOF)
	{
		if(' ' == c)
			++nb;
		if('\t' == c)
			++nt;
		if('\n' == c)
			++nn;
	}

	printf("There are %d blanks; %d tabs; %d newlines \n", nb, nt, nn);
}
