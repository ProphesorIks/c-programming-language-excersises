#include <stdio.h>

#define IN 1
#define OUT 0
/*This program outputs input words one word per line*/
int main()
{
	int c = 0;
	int state = OUT;

	while((c = getchar()) != EOF) 
	{
		if(c == ' ' || c == '\t' || c == '\n')
		{
			if(state != OUT)
			{
				putchar('\n');
				state = OUT;
			}
		}
		else
		{
			if(state != IN)
				state = IN;
			putchar(c);
		}	
	}
}
