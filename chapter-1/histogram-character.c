#include <stdio.h>
#define CHAR_START '!'
#define CHAR_END '~'
#define CHAR_SET_SIZE CHAR_END - CHAR_START
/*this program prints a histogram of characters*/

int main()
{
	int c = 0;
	int carr[CHAR_SET_SIZE];
	
	for(int i = 0; i < CHAR_SET_SIZE; i++)
	{
		carr[i] = 0;
	}

	while((c = getchar()) != EOF)
	{
		if(CHAR_START <= c && CHAR_END >= c)
		{
			carr[c - CHAR_START]++;
		}
	}

	/* print the table*/
	for(int i = 0; i < CHAR_SET_SIZE; i++)
	{
		if(0 != carr[i])
		{
			printf("%c:", i + CHAR_START);
			for(int j = 0; j < carr[i]; j++)
			{
				printf("-");
			}
			printf("\n");
		}
	}	
}
