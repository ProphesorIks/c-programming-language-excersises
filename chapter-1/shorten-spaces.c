#include <stdio.h>
#include <stdbool.h>

int main()
{
	int c;
	bool is_previous_space = false;
	while((c = getchar()) != EOF)
	{
		if(c != ' ')
		{
			putchar(c);
			is_previous_space = false;
		}
		else
		{
			if(!is_previous_space)
			{
				putchar(c);
			}
			is_previous_space = true;
		}
	}
}
