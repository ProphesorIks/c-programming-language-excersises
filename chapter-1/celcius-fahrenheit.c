#include <stdio.h>

int main()
{
	int celcius = 0;

	for(celcius = 0; celcius <= 200; celcius += 20){
		printf("%3d %6.0f\n", celcius, celcius * (9.0 / 5.0) + 32);

	}
}
